﻿using Synapse.Metier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Synapse.Utilitaire
{
    public static class Donnees
    {
        private static List<Intervenant> _collectionIntervenant;
        private static List<Projet> _collectionProjet;

        
        public static List<Intervenant> CollectionIntervenant
        {
            get
            {
                if (_collectionIntervenant == null)
                {
                    _collectionIntervenant = (List<Intervenant>)Persistance.ChargerDonnees("Intervenant");
                    if (_collectionIntervenant == null)
                        _collectionIntervenant = new List<Intervenant>();
                }
                return Donnees._collectionIntervenant;
            }
            set { Donnees._collectionIntervenant = value; }
        }
      
        public static List<Projet> CollectionProjet
        {
            get
            {
                if (_collectionProjet == null)
                {
                    _collectionProjet = (List<Projet>)Persistance.ChargerDonnees("Projet");
                    if (_collectionProjet == null)
                        _collectionProjet = new List<Projet>();
                }
                return Donnees._collectionProjet;
            }
        }
        
        public static void SauvegardeDonnees()
        {
            Persistance.SauvegarderDonnees("Intervenant", _collectionIntervenant);
            Persistance.SauvegarderDonnees("Projet", _collectionProjet);
        }


    }
}
