﻿namespace Synapse.Vues
{
    partial class FormFenetrePrincipale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menupPincipaleMenuStrip = new System.Windows.Forms.MenuStrip();
            this.intervenantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aJOUTINTREVENANTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lISTEINTERVENANTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aJOUTPROJETToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menupPincipaleMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menupPincipaleMenuStrip
            // 
            this.menupPincipaleMenuStrip.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menupPincipaleMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.intervenantToolStripMenuItem,
            this.projetToolStripMenuItem});
            this.menupPincipaleMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.menupPincipaleMenuStrip.Name = "menupPincipaleMenuStrip";
            this.menupPincipaleMenuStrip.Size = new System.Drawing.Size(284, 24);
            this.menupPincipaleMenuStrip.TabIndex = 0;
            this.menupPincipaleMenuStrip.Text = "menuStrip1";
            // 
            // intervenantToolStripMenuItem
            // 
            this.intervenantToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aJOUTINTREVENANTToolStripMenuItem,
            this.lISTEINTERVENANTToolStripMenuItem});
            this.intervenantToolStripMenuItem.Name = "intervenantToolStripMenuItem";
            this.intervenantToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.intervenantToolStripMenuItem.Text = "Intervenant";
            // 
            // projetToolStripMenuItem
            // 
            this.projetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aJOUTPROJETToolStripMenuItem});
            this.projetToolStripMenuItem.Name = "projetToolStripMenuItem";
            this.projetToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.projetToolStripMenuItem.Text = "Projet";
            // 
            // aJOUTINTREVENANTToolStripMenuItem
            // 
            this.aJOUTINTREVENANTToolStripMenuItem.Name = "aJOUTINTREVENANTToolStripMenuItem";
            this.aJOUTINTREVENANTToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.aJOUTINTREVENANTToolStripMenuItem.Text = "AJOUT INTREVENANT";
            // 
            // lISTEINTERVENANTToolStripMenuItem
            // 
            this.lISTEINTERVENANTToolStripMenuItem.Name = "lISTEINTERVENANTToolStripMenuItem";
            this.lISTEINTERVENANTToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.lISTEINTERVENANTToolStripMenuItem.Text = "LISTE INTERVENANT";
            // 
            // aJOUTPROJETToolStripMenuItem
            // 
            this.aJOUTPROJETToolStripMenuItem.Name = "aJOUTPROJETToolStripMenuItem";
            this.aJOUTPROJETToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.aJOUTPROJETToolStripMenuItem.Text = "AJOUT PROJET";
            // 
            // FormFenetrePrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.menupPincipaleMenuStrip);
            this.MainMenuStrip = this.menupPincipaleMenuStrip;
            this.Name = "FormFenetrePrincipale";
            this.Text = "FormFenetrePrincipale";
            this.Load += new System.EventHandler(this.FormFenetrePrincipale_Load);
            this.menupPincipaleMenuStrip.ResumeLayout(false);
            this.menupPincipaleMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menupPincipaleMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem intervenantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aJOUTINTREVENANTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lISTEINTERVENANTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aJOUTPROJETToolStripMenuItem;
    }
}