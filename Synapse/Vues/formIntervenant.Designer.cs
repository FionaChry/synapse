﻿namespace Synapse.Vues
{
    partial class formAjoutIntervenant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonAjoutIntervenant = new System.Windows.Forms.Button();
            this.textBoxTuaxHoraire = new System.Windows.Forms.TextBox();
            this.textBoxNomIntervenant = new System.Windows.Forms.TextBox();
            this.labelTauxHoraire = new System.Windows.Forms.Label();
            this.labelNomIntervenant = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonAjoutIntervenant);
            this.groupBox1.Controls.Add(this.textBoxTuaxHoraire);
            this.groupBox1.Controls.Add(this.textBoxNomIntervenant);
            this.groupBox1.Controls.Add(this.labelTauxHoraire);
            this.groupBox1.Controls.Add(this.labelNomIntervenant);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 180);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Intervenant";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // buttonAjoutIntervenant
            // 
            this.buttonAjoutIntervenant.Location = new System.Drawing.Point(249, 115);
            this.buttonAjoutIntervenant.Name = "buttonAjoutIntervenant";
            this.buttonAjoutIntervenant.Size = new System.Drawing.Size(115, 41);
            this.buttonAjoutIntervenant.TabIndex = 4;
            this.buttonAjoutIntervenant.Text = "Ajoutez";
            this.buttonAjoutIntervenant.UseVisualStyleBackColor = true;
            this.buttonAjoutIntervenant.Click += new System.EventHandler(this.buttonAjoutIntervenant_Click);
            // 
            // textBoxTuaxHoraire
            // 
            this.textBoxTuaxHoraire.Location = new System.Drawing.Point(148, 65);
            this.textBoxTuaxHoraire.Name = "textBoxTuaxHoraire";
            this.textBoxTuaxHoraire.Size = new System.Drawing.Size(115, 20);
            this.textBoxTuaxHoraire.TabIndex = 3;
            this.textBoxTuaxHoraire.TextChanged += new System.EventHandler(this.textBoxTuaxHoraire_TextChanged);
            // 
            // textBoxNomIntervenant
            // 
            this.textBoxNomIntervenant.Location = new System.Drawing.Point(148, 25);
            this.textBoxNomIntervenant.Name = "textBoxNomIntervenant";
            this.textBoxNomIntervenant.Size = new System.Drawing.Size(216, 20);
            this.textBoxNomIntervenant.TabIndex = 2;
            this.textBoxNomIntervenant.TextChanged += new System.EventHandler(this.textBoxNomIntervenant_TextChanged);
            // 
            // labelTauxHoraire
            // 
            this.labelTauxHoraire.AutoSize = true;
            this.labelTauxHoraire.Location = new System.Drawing.Point(15, 72);
            this.labelTauxHoraire.Name = "labelTauxHoraire";
            this.labelTauxHoraire.Size = new System.Drawing.Size(68, 13);
            this.labelTauxHoraire.TabIndex = 1;
            this.labelTauxHoraire.Text = "Taux Horaire";
            // 
            // labelNomIntervenant
            // 
            this.labelNomIntervenant.AutoSize = true;
            this.labelNomIntervenant.Location = new System.Drawing.Point(6, 32);
            this.labelNomIntervenant.Name = "labelNomIntervenant";
            this.labelNomIntervenant.Size = new System.Drawing.Size(86, 13);
            this.labelNomIntervenant.TabIndex = 0;
            this.labelNomIntervenant.Text = "Nom Intervenant";
            // 
            // formAjoutIntervenant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 204);
            this.Controls.Add(this.groupBox1);
            this.Name = "formAjoutIntervenant";
            this.Text = "AJOUT INTERVENANT";
            this.Load += new System.EventHandler(this.formIntervenant_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxNomIntervenant;
        private System.Windows.Forms.Label labelTauxHoraire;
        private System.Windows.Forms.Label labelNomIntervenant;
        private System.Windows.Forms.Button buttonAjoutIntervenant;
        private System.Windows.Forms.TextBox textBoxTuaxHoraire;
    }
}