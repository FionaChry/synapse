﻿namespace Synapse.Vues
{
    partial class formListeIntervenant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Liste = new System.Windows.Forms.GroupBox();
            this.dataGridViewListeAgent = new System.Windows.Forms.DataGridView();
            this.Liste.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeAgent)).BeginInit();
            this.SuspendLayout();
            // 
            // Liste
            // 
            this.Liste.Controls.Add(this.dataGridViewListeAgent);
            this.Liste.Location = new System.Drawing.Point(13, 13);
            this.Liste.Name = "Liste";
            this.Liste.Size = new System.Drawing.Size(259, 237);
            this.Liste.TabIndex = 0;
            this.Liste.TabStop = false;
            this.Liste.Text = "Liste";
            // 
            // dataGridViewListeAgent
            // 
            this.dataGridViewListeAgent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListeAgent.Location = new System.Drawing.Point(17, 20);
            this.dataGridViewListeAgent.Name = "dataGridViewListeAgent";
            this.dataGridViewListeAgent.Size = new System.Drawing.Size(225, 200);
            this.dataGridViewListeAgent.TabIndex = 0;
            this.dataGridViewListeAgent.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // formListeIntervenant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.Liste);
            this.Name = "formListeIntervenant";
            this.Text = "Liste Intervenant";
            this.Liste.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeAgent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Liste;
        private System.Windows.Forms.DataGridView dataGridViewListeAgent;
    }
}