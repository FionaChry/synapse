﻿using System;
using Synapse.Metier;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Utilitaire;
using System.Windows.Forms;

namespace Synapse.Vues
{
    public partial class formAjoutIntervenant : Form
    {
        public formAjoutIntervenant()
        {
            InitializeComponent();
        }

        private bool VerificationFormulaireAjoutIntervenant()
        {
            bool resultat = false;
            if(this.textBoxNomIntervenant.Text=="")
            {
                MessageBox.Show("Saisir lr nom de l'intervenant");
            }
            else if (this.textBoxTuaxHoraire.Text=="")
            {
                MessageBox.Show("Saisir le taux horaire de l'intervenant");
            }
            else
            {
                resultat = true;
            }
            return resultat;
        }

        private void formIntervenant_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBoxNomIntervenant_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxTuaxHoraire_TextChanged(object sender, EventArgs e)
        {

        }
               
        private void buttonAjoutIntervenant_Click(object sender, EventArgs e)
        {
            if(VerificationFormulaireAjoutIntervenant())
            {
                string _nom = textBoxNomIntervenant.Text;
                decimal _tauxHoraire = decimal.Parse(textBoxTuaxHoraire.Text);
                Intervenant unIntervenant = new Intervenant(_nom, _tauxHoraire);

                Donnees.CollectionIntervenant.Add(unIntervenant);

            }
        }

       
        
    }
}
