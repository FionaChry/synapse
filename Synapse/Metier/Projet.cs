﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    [Serializable()]
    public class Projet
    {
        private string _nom;
        private DateTime _debut;
        private DateTime _fin;
        private decimal _prixFactureMO;

        private List<Mission> _missions;

        private decimal CumulCoutMO()
        {
            decimal cout = 0;
            foreach (Mission cumulCout in _missions)
            {
               Intervenant I= cumulCout.Executant;
                int m = cumulCout.NbHeuresEffectuees();

                cout = cout + I.TauxHoraire * m;

            }
            return cout;
        }
        public decimal MargeBruteCourante()
        {
            decimal marge = 0;
            decimal cumul = this.CumulCoutMO();
            decimal prixFacture = this._prixFactureMO;
            marge = marge + prixFacture - cumul;
            return marge;
        }
        
    }
}
