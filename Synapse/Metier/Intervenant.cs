﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    [Serializable()]
    public class Intervenant
    {
        private string _nom;
        private decimal _tauxHoraire;

        public Intervenant(string _nom, decimal _tauxHoraire)
        {
            this._nom = _nom;
            this._tauxHoraire = _tauxHoraire;
        }

        public string Nom
        {
            get{return _nom;}
        }

        public decimal TauxHoraire
        {
            get { return _tauxHoraire;}
        }
       
    }
}
