﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    [Serializable()]
    public class Mission
    {
        private string _nom;
        private string _description;
        private int _nbHeuresPrevues;
        private Dictionary<DateTime, int> _releveHoraire;
        private Intervenant _executant;

        public Dictionary<DateTime, int> ReleveHoraire
        {
            get{return _releveHoraire;}
        }

        internal Intervenant Executant
        {
            get{ return _executant;}
        }

        public void AjouteReleve(DateTime date, int nbHeures)
        {
            _releveHoraire.Add(date, nbHeures);
        }
        public int NbHeuresEffectuees()
        {
            int resultat = 0;
            /*foreach (int valeurCourante in _releveHoraire.Values)
            {
                resultat = resultat + valeurCourante;
            }*/
            foreach (KeyValuePair<DateTime,int> releveCourant in _releveHoraire)
            {
                resultat = resultat + releveCourant.Value;
            }
            return resultat;
        }
    }
}
